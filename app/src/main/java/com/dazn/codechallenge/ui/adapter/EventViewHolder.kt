package com.dazn.codechallenge.ui.adapter

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.RequestManager
import com.dazn.codechallenge.databinding.ItemEventBinding
import com.dazn.codechallenge.network.response.Event

class EventViewHolder(private val binding: ItemEventBinding, private val glide: RequestManager) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(event: Event, onClickListener: ((Event) -> Unit)?) {
        glide.load(event.imageUrl).into(binding.imageViewThumbnail)
        binding.eventItem = event
        binding.executePendingBindings()
    }
}