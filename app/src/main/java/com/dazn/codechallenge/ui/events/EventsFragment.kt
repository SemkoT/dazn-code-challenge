package com.dazn.codechallenge.ui.events

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.dazn.codechallenge.R
import com.dazn.codechallenge.databinding.FragmentEventsBinding
import com.dazn.codechallenge.network.Status
import com.dazn.codechallenge.ui.adapter.EventsAdapter
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class EventsFragment : Fragment(R.layout.fragment_events) {

    private lateinit var binding: FragmentEventsBinding
    lateinit var eventsViewModel: EventsViewModel

    @Inject
    lateinit var eventsAdapter: EventsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentEventsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        eventsViewModel = ViewModelProvider(requireActivity()).get(EventsViewModel::class.java)
        setupRecyclerView()
        subscribeToObservers()
        setupSwipeToRefresh()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupRecyclerView() {
        binding.recyclerViewEvents.apply {
            adapter = eventsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }
    }

    private fun subscribeToObservers() {
        eventsViewModel.events.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    showProgressBar()
                }
                Status.ERROR -> {
                    hideProgressBar()
                    Snackbar.make(requireView(), "An Error occurred :/", Snackbar.LENGTH_LONG).show()
                }
                Status.SUCCESS -> {
                    hideProgressBar()
                    it.data?.let { events ->
                        eventsAdapter.submitList(events)
                    }
                }
            }
        })
    }

    private fun showProgressBar() {
        binding.swipeToRefresh.isRefreshing = true
    }

    private fun hideProgressBar() {
        binding.swipeToRefresh.isRefreshing = false
    }

    private fun setupSwipeToRefresh() {
        binding.swipeToRefresh.setOnRefreshListener {
            eventsViewModel.onPullToRefreshPulled()
        }
    }
}