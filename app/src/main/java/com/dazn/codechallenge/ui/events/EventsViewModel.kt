package com.dazn.codechallenge.ui.events

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.response.Event
import com.dazn.codechallenge.repository.DaznRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class EventsViewModel @Inject constructor(private val daznRepository: DaznRepository) :
    ViewModel() {

    private val eventsMutable = MutableLiveData<Resource<List<Event>>>()
    val events: LiveData<Resource<List<Event>>> = eventsMutable

    init {
        getEvents()
    }

    private fun getEvents() = viewModelScope.launch {
        eventsMutable.postValue(Resource.loading())
        val response = daznRepository.getEvents()
        eventsMutable.postValue(response)
    }

    fun onPullToRefreshPulled() {
        getEvents()
    }

}