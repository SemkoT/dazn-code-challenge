package com.dazn.codechallenge.ui.schedule

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.SimpleItemAnimator
import com.dazn.codechallenge.R
import com.dazn.codechallenge.databinding.FragmentScheduleBinding
import com.dazn.codechallenge.network.Status
import com.dazn.codechallenge.ui.adapter.EventsAdapter
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ScheduleFragment : Fragment(R.layout.fragment_schedule) {

    private lateinit var binding: FragmentScheduleBinding
    lateinit var scheduleViewModel: ScheduleViewModel

    @Inject
    lateinit var eventsAdapter: EventsAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentScheduleBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        scheduleViewModel = ViewModelProvider(requireActivity()).get(ScheduleViewModel::class.java)
        setupRecyclerView()
        subscribeToObservers()
        setupSwipeToRefresh()
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setupRecyclerView() {
        binding.recyclerViewSchedule.apply {
            adapter = eventsAdapter
            layoutManager = LinearLayoutManager(requireContext())
            (itemAnimator as? SimpleItemAnimator)?.supportsChangeAnimations = false
        }
    }

    private fun subscribeToObservers() {
        scheduleViewModel.schedule.observe(viewLifecycleOwner, {
            when (it.status) {
                Status.LOADING -> {
                    showProgressBar()
                }
                Status.ERROR -> {
                    hideProgressBar()
                    Snackbar.make(requireView(), "An Error occurred :/", Snackbar.LENGTH_LONG).show()
                }
                Status.SUCCESS -> {
                    hideProgressBar()
                    it.data?.let { events ->
                        eventsAdapter.submitList(events)
                    }
                }
            }
        })
    }

    private fun showProgressBar() {
        binding.swipeToRefresh.isRefreshing = true
    }

    private fun hideProgressBar() {
        binding.swipeToRefresh.isRefreshing = false
    }

    private fun setupSwipeToRefresh() {
        binding.swipeToRefresh.setOnRefreshListener {
            scheduleViewModel.onPullToRefreshPulled()
        }
    }

    override fun onResume() {
        scheduleViewModel.onResume()
        super.onResume()
    }

    override fun onPause() {
        scheduleViewModel.onPause()
        super.onPause()
    }
}