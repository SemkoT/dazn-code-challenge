package com.dazn.codechallenge.ui.schedule

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dazn.codechallenge.Constants.SCHEDULE_REFRESH_PERIOD_MS
import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.response.Event
import com.dazn.codechallenge.repository.DaznRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ScheduleViewModel @Inject constructor(
    private val daznRepository: DaznRepository
) : ViewModel() {

    private val TAG = "ScheduleViewModel"

    private val scheduleMutable = MutableLiveData<Resource<List<Event>>>()
    val schedule: LiveData<Resource<List<Event>>> = scheduleMutable

    var refreshJob: Job? = null

    var refreshPeriod = SCHEDULE_REFRESH_PERIOD_MS

    private fun launchRefreshJob() = viewModelScope.launch {
        Log.d(TAG, "Launch refresh job")
        while (true) {
            Log.d(TAG, "Refresh job running")
            getSchedule()
            delay(refreshPeriod)
        }
    }

    private suspend fun getSchedule() {
        scheduleMutable.postValue(Resource.loading())
        val response = daznRepository.getSchedule()
        scheduleMutable.postValue(response)
    }

    fun onPullToRefreshPulled() = viewModelScope.launch {
        getSchedule()
    }

    fun onResume() {
        refreshJob = launchRefreshJob()
    }

    fun onPause() {
        Log.d(TAG, "Cancel refresh job")
        refreshJob?.cancel()
    }

}