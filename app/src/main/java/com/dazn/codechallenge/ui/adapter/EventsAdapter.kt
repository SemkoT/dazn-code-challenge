package com.dazn.codechallenge.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.RequestManager
import com.dazn.codechallenge.databinding.ItemEventBinding
import com.dazn.codechallenge.network.response.Event
import javax.inject.Inject

class EventsAdapter @Inject constructor(private val glide: RequestManager) :
    ListAdapter<Event, EventViewHolder>(differCallback) {
    companion object {
        val differCallback = object : DiffUtil.ItemCallback<Event>() {
            override fun areItemsTheSame(oldItem: Event, newItem: Event): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: Event, newItem: Event): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemEventBinding.inflate(layoutInflater, parent, false)
        return EventViewHolder(itemBinding, glide)
    }

    override fun onBindViewHolder(holder: EventViewHolder, position: Int) {
        val shoppingItem = getItem(position)
        holder.bind(shoppingItem, onClickListener)
    }

    private var onClickListener: ((Event) -> Unit)? = null

    fun setOnItemClickListener(listener: (Event) -> Unit) {
        onClickListener = listener
    }
}