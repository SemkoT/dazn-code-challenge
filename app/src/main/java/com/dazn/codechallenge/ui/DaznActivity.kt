package com.dazn.codechallenge.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.dazn.codechallenge.R
import com.dazn.codechallenge.databinding.ActivityDaznBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DaznActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDaznBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bindView()
        initializeBottomNavigation()
    }

    private fun bindView() {
        binding = ActivityDaznBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun initializeBottomNavigation() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.newsNavHostFragment) as NavHostFragment
        binding.bottomNavigationView.setupWithNavController(navHostFragment.navController)
    }
}