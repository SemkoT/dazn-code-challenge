package com.dazn.codechallenge.network

import com.dazn.codechallenge.network.response.Event
import retrofit2.Response
import retrofit2.http.GET


interface DaznAPI {

    @GET("/getEvents")
    suspend fun getEvents(): Response<List<Event>>

    @GET("/getSchedule")
    suspend fun getSchedule(): Response<List<Event>>
}