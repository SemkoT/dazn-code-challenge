package com.dazn.codechallenge.di

import android.content.Context
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.dazn.codechallenge.Constants.DAZN_URL
import com.dazn.codechallenge.R
import com.dazn.codechallenge.network.DaznAPI
import com.dazn.codechallenge.repository.DaznRepository
import com.dazn.codechallenge.repository.DefaultDaznRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun provideDaznApi(): DaznAPI =
        Retrofit.Builder().addConverterFactory(GsonConverterFactory.create())
            .baseUrl(DAZN_URL).build().create(DaznAPI::class.java)

    @Singleton
    @Provides
    fun provideDefaultDaznRepository(daznAPI: DaznAPI) =
        DefaultDaznRepository(daznAPI) as DaznRepository

    @Singleton
    @Provides
    fun provideGlide(@ApplicationContext applicationContext: Context) =
        Glide.with(applicationContext).setDefaultRequestOptions(
            RequestOptions().placeholder(R.drawable.ic_image).error(R.drawable.ic_image)
                .dontAnimate().diskCacheStrategy(DiskCacheStrategy.ALL)
        )
}