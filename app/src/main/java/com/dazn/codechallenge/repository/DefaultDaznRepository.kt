package com.dazn.codechallenge.repository

import com.dazn.codechallenge.network.DaznAPI
import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.response.Event
import javax.inject.Inject

class DefaultDaznRepository @Inject constructor(
    private val daznAPI: DaznAPI
) : DaznRepository {

    override suspend fun getEvents(): Resource<List<Event>> {
        return try {
            val response = daznAPI.getEvents()
            if (response.isSuccessful) {
                response.body()?.let {
                    return@let Resource.success(it)
                } ?: Resource.error("An unknown error. Empty data.")
            } else {
                Resource.error(
                    "An error occured: ${response.message()}, with code: ${response.code()}"
                )
            }
        } catch (e: Exception) {
            Resource.error("Couldn't reach the server", null)
        }
    }

    override suspend fun getSchedule(): Resource<List<Event>> {
        return try {
            val response = daznAPI.getSchedule()
            if (response.isSuccessful) {
                response.body()?.let {
                    return@let Resource.success(it)
                } ?: Resource.error("An unknown error. Empty data.")
            } else {
                Resource.error(
                    "An error occured: ${response.message()}, with code: ${response.code()}"
                )
            }
        } catch (e: Exception) {
            Resource.error("Couldn't reach the server")
        }
    }
}