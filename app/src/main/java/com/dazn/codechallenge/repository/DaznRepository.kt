package com.dazn.codechallenge.repository

import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.response.Event

interface DaznRepository {

    suspend fun getEvents(): Resource<List<Event>>

    suspend fun getSchedule(): Resource<List<Event>>
}