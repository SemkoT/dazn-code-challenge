package com.dazn.codechallenge

object Constants{
    const val DAZN_URL = "https://us-central1-dazn-sandbox.cloudfunctions.net"
    const val SCHEDULE_REFRESH_PERIOD_MS = 10000L
}