package com.dazn.codechallenge.repository

import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.response.Event
import kotlinx.coroutines.delay

class FakeDaznRepository : DaznRepository {

    var shouldReturnNetworkError = false

    var responseData: List<Event>? = null

    override suspend fun getEvents(): Resource<List<Event>> {
        delay(100)
        return if (shouldReturnNetworkError.not()) {
            Resource.success(responseData)
        } else {
            Resource.error("Test error", null)
        }
    }

    override suspend fun getSchedule(): Resource<List<Event>> {
        delay(100)
        return if (shouldReturnNetworkError.not()) {
            Resource.success(responseData)
        } else {
            Resource.error("Test error", null)
        }
    }
}