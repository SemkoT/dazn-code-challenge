package com.dazn.codechallenge.repository

import com.dazn.codechallenge.network.response.Event


class FakeDataProvider {
    companion object {
        val data = listOf(
            Event("123", "1", "asdf", "qwer", "zxcv"),
            Event("123", "2", "asdf", "qwer", "zxcv"),
            Event("123", "3", "asdf", "qwer", "zxcv")
        )
    }
}
