package com.dazn.codechallenge.ui.schedule

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dazn.codechallenge.MainCoroutineRule
import com.dazn.codechallenge.getOrAwaitValueTest
import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.Status
import com.dazn.codechallenge.network.response.Event
import com.dazn.codechallenge.repository.FakeDataProvider
import com.dazn.codechallenge.repository.FakeDaznRepository
import com.google.common.truth.Truth.assertThat
import com.nhaarman.mockitokotlin2.times
import com.nhaarman.mockitokotlin2.verify
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock


@ExperimentalCoroutinesApi
class ScheduleViewModelTest {

    //make sure everything runs in the same thread
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: ScheduleViewModel
    private lateinit var fakeDaznRepository: FakeDaznRepository

    @Before
    fun setup() {
        fakeDaznRepository = FakeDaznRepository()
        viewModel = ScheduleViewModel(fakeDaznRepository)
    }

    @Test
    fun `pull to refresh with simulated network error, schedule LiveData posts Error`() {
        var firstResponse: Resource<List<Event>>? = null
        var secondResponse: Resource<List<Event>>? = null

        mainCoroutineRule.launch {
            fakeDaznRepository.shouldReturnNetworkError = true
            viewModel.onPullToRefreshPulled()
            firstResponse = viewModel.schedule.getOrAwaitValueTest()
            mainCoroutineRule.advanceUntilIdle()
            secondResponse = viewModel.schedule.getOrAwaitValueTest()

        }

        assertThat(firstResponse?.status).isEqualTo(Status.LOADING)
        assertThat(secondResponse?.status).isEqualTo(Status.ERROR)
    }

    @Test
    fun `pull to refresh, schedule LiveData posts Success`() {
        var firstResponse: Resource<List<Event>>? = null
        var secondResponse: Resource<List<Event>>? = null
        fakeDaznRepository.responseData = FakeDataProvider.data

        mainCoroutineRule.launch {
            viewModel.onPullToRefreshPulled()
            firstResponse = viewModel.schedule.getOrAwaitValueTest()
            mainCoroutineRule.advanceUntilIdle()
            secondResponse = viewModel.schedule.getOrAwaitValueTest()
        }

        assertThat(firstResponse?.status).isEqualTo(Status.LOADING)
        assertThat(secondResponse?.status).isEqualTo(Status.SUCCESS)
        assertThat(secondResponse?.data?.size).isEqualTo(FakeDataProvider.data.size)
    }

    @Test
    fun `onResume, refresh job running`() {
        var stateAfterOnResume: Boolean = false
        viewModel.refreshPeriod = 50

        mainCoroutineRule.launch {
            viewModel.onResume()
            stateAfterOnResume = viewModel.refreshJob?.isActive ?: false
            viewModel.onPause()
        }

        assertThat(stateAfterOnResume).isTrue()
    }

    @Test
    fun `onResume onPause, refresh job is canceled`() {
        var stateAfterOnPause: Boolean = false
        viewModel.refreshPeriod = 50

        mainCoroutineRule.launch {
            viewModel.onResume()
            viewModel.onPause()
            stateAfterOnPause = viewModel.refreshJob?.isCancelled ?: false
        }

        assertThat(stateAfterOnPause).isTrue()
    }

    @Test
    fun `onResume onPause, getSchedule is called once`() {
        fakeDaznRepository = mock(FakeDaznRepository::class.java)
        viewModel = ScheduleViewModel(fakeDaznRepository)
        viewModel.refreshPeriod = 50

        mainCoroutineRule.launch {
            viewModel.onResume()
            viewModel.onPause()
            verify(fakeDaznRepository, times(1)).getSchedule()
        }
    }

    @Test
    fun `onResume onPause x2, getSchedule is called twice`() {
        fakeDaznRepository = mock(FakeDaznRepository::class.java)
        viewModel = ScheduleViewModel(fakeDaznRepository)
        viewModel.refreshPeriod = 50

        mainCoroutineRule.launch {
            viewModel.onResume()
            viewModel.onPause()
            viewModel.onResume()
            viewModel.onPause()
            verify(fakeDaznRepository, times(2)).getSchedule()
        }
    }
}