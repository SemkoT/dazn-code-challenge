package com.dazn.codechallenge.ui.events

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.dazn.codechallenge.MainCoroutineRule
import com.dazn.codechallenge.getOrAwaitValueTest
import com.dazn.codechallenge.network.Resource
import com.dazn.codechallenge.network.Status
import com.dazn.codechallenge.network.response.Event
import com.dazn.codechallenge.repository.FakeDataProvider
import com.dazn.codechallenge.repository.FakeDaznRepository
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test


@ExperimentalCoroutinesApi
class EventsViewModelTest {

    //make sure everything runs in the same thread
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: EventsViewModel
    private lateinit var fakeDaznRepository: FakeDaznRepository

    @Before
    fun setup() {
        fakeDaznRepository = FakeDaznRepository()
        viewModel = EventsViewModel(fakeDaznRepository)
    }

    @Test
    fun `pull to refresh with simulated network error, schedule LiveData posts Error`() {
        var firstResponse: Resource<List<Event>>? = null
        var secondResponse: Resource<List<Event>>? = null

        mainCoroutineRule.launch {
            fakeDaznRepository.shouldReturnNetworkError = true
            viewModel.onPullToRefreshPulled()
            firstResponse = viewModel.events.getOrAwaitValueTest()
            mainCoroutineRule.advanceUntilIdle()
            secondResponse = viewModel.events.getOrAwaitValueTest()

        }

        assertThat(firstResponse?.status).isEqualTo(Status.LOADING)
        assertThat(secondResponse?.status).isEqualTo(Status.ERROR)
    }

    @Test
    fun `pull to refresh, schedule LiveData posts Success`() {
        var firstResponse: Resource<List<Event>>? = null
        var secondResponse: Resource<List<Event>>? = null
        fakeDaznRepository.responseData = FakeDataProvider.data

        mainCoroutineRule.launch {
            viewModel.onPullToRefreshPulled()
            firstResponse = viewModel.events.getOrAwaitValueTest()
            mainCoroutineRule.advanceUntilIdle()
            secondResponse = viewModel.events.getOrAwaitValueTest()
        }

        assertThat(firstResponse?.status).isEqualTo(Status.LOADING)
        assertThat(secondResponse?.status).isEqualTo(Status.SUCCESS)
        assertThat(secondResponse?.data?.size).isEqualTo(FakeDataProvider.data.size)
    }

}